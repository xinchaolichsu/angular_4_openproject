import { Component, OnInit } from '@angular/core';
import 'jquery';
import 'materialize-css';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: "dashboard.component.html",
    styleUrls: [
    	"dashboard.component.css",
    ]
})

export class Dashboard implements OnInit {
    ngOnInit(): void {}
    link_logo = '../assets/image/materialize-logo.png';

    updatefield() {
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        alert('ok');
    }
}
