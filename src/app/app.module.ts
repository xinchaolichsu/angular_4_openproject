import { BrowserModule } from '@angular/platform-browser';
import { AppRouterModule } from './app-router.module';
import { NgModule } from '@angular/core';
import { Dashboard } from './Dashboard/dashboard.component';
import { CheckboxComponent } from './Dashboard/checkbox/checkbox.component';
import { RadioComponent } from './Dashboard/Radio/radio.component';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent,
        Dashboard,
        CheckboxComponent,
        RadioComponent
    ],
    imports: [
        BrowserModule,
        AppRouterModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
