import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Dashboard } from './Dashboard/dashboard.component';
import { CheckboxComponent } from './Dashboard/checkbox/checkbox.component';
import { RadioComponent } from './Dashboard/Radio/radio.component';

const appRoutes : Routes = [
	{
	    path: '',
	    component: Dashboard
	}, 
	{
	    path: 'checkbox',
	    component: CheckboxComponent,
	}, 
	{
	    path: 'radio',
	    component: RadioComponent
	},
	{
		path: '',
		redirectTo: '/sub-component',
	    pathMatch: 'full'
	},
	{
		path: 'sub-component',
		component: Dashboard,
		children: [
			{
			    path: 'checkbox',
			    component: CheckboxComponent,
			}, 
		]
	}
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRouterModule {}