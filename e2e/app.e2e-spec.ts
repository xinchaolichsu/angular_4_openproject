import { OpenprojectPage } from './app.po';

describe('openproject App', () => {
  let page: OpenprojectPage;

  beforeEach(() => {
    page = new OpenprojectPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
